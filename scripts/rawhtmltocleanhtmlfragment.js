// Reads   ./rawcurl.html
// Writes  ./htmlfragmentoutput.html and ./images.urls
// Extracts essentials from 3rd party generated original blog html feed
const fs = require('fs');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;
var rawhtml = "";
var cleanhtmlstring = "";
var FEED_IMAGE_URLS = [];

fs.readFile('./rawcurl.html', (error, data) => { if ( error ) {console.error(error); return;} rawhtml=data.toString();
  const dom = new JSDOM(rawhtml)
  dom.window.document.querySelectorAll('article').forEach(function(articl,num,z){
    var
    POST_ID = articl.id ,
    POST_TITLE = articl.querySelector('article h2').textContent ,
    POST_ORIG_IMG = articl.querySelector('article figure img').getAttribute('data-orig-file') ,
    POST_ORIG_IMG_2 = articl.querySelector('article .entry-content').removeChild(articl.querySelector('figure.wp-block-image')).getAttribute('data-orig-file'),
    POST_CONTENTS = articl.querySelector('article .entry-content').innerHTML ,
    POST_TIMES_STRING = "🕰️" + articl.querySelector('article time.published').textContent;
    var cleanarticletemplateliteral = `<article id="${POST_ID}">
    <header class="entry-header responsive-max-width">
    <h2 class="entry-title">${POST_TITLE}</h2>
    </header>
    <div class="entry-content">
    <figure>
    <img src="${POST_ORIG_IMG}">
    </figure>
    <p>${POST_CONTENTS}</p>
    </div>
    <footer class="entry-footer responsive-max-width">
    <span class="posted-on">
    <time class="entry-date published">
    ${POST_TIMES_STRING}
    </time>
    </span>
    </footer>
    </article>`;
    cleanhtmlstring += cleanarticletemplateliteral;
    FEED_IMAGE_URLS.push(POST_ORIG_IMG);
  })
    fs.writeFile('./htmlfragmentoutput.html', cleanhtmlstring, function(err){if(err)return console.log(err)})
    fs.writeFile('./images.urls', FEED_IMAGE_URLS.join("\n")+"\n", function(err){if(err)return console.log(err)})
})
