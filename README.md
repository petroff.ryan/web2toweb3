# Web2 to Web3

## Web2toWeb3 is an infrastructure tool for republishing content securely to Web3. 
Hosted at https://gitlab.com/petroff.ryan/web2toweb3/ , this repo holds public copies of the Web2toWeb3 publishing scripts. 

Web3 is "a decentralized and fair internet where users control their own data, identity and destiny." ( https://web3.foundation/about/ )

![Web2toWeb3 Dataflow Diagram 0.8.1](/docs/v1.3.jpg)

Web2toWeb3 is operational and live right now! Our development server publishes tastytreasures.eth for testing the ipfs.io/ipns/ gateway: https://ipfs.io/ipns/tastytreasures.eth . A snapshot is rehosted at http://petroff.ryan.gitlab.io/static-hosted-site-tastytreasures/ in case of gateway problems. ( Erroring 'ipfs resolve -r /ipns/tastytreasures.eth: could not resolve name: "tastytreasures.eth" is missing a DNSLink record' is the expected gateway failure mode. )

This project uses advanced technologies, so please don't be discouraged if something doesn't work or is difficult to understand the first time through.

I hope you will contact me if you have any questions, comments, or improvements to suggest. :) 



# Tasty Treasures and the tale of the Immortal Blog

First, a poem and a zen koan

     We shall not cease from exploration
      And the end of all our exploring
       Will be to arrive where we started
        And know the place for the first time.
                        ~ exerpt from T. S. Eliot's Little Gidding

     Before enlightenment; chop wood, carry water. 
      After enlightenment; chop wood, carry water.
                            ~ zen koan


This project starts with website and ends with a website.

To understand what's going on here, you need to know about some of the decentralized technologies that are collectively referred to as 'Web3'.

We will be using Ethereum and IPFS, along with Git and a software forge. 

Books have been written explaining each, but I'll allow the technologies to speak for themselves briefly.

 - First, Ethereum: ( from ethereum.org )

"Ethereum is the community-run technology powering the cryptocurrency ether (ETH) and thousands of decentralized applications."

 - Second, ENS: ( from ens.domains )

"The Ethereum Name Service is an open source blockchain-based naming protocol."

 - Third, IPFS: ( from ipfs.io )

"IPFS (the InterPlanetary File System) is a peer-to-peer network and protocol designed to make the web faster, safer, and more open. IPFS upgrades the web to work peer to peer, addressing data by what it is instead of where it’s located on the network, or who is hosting it."
  
 - Fourth, Git: ( from git-scm.com )

"Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency."

 - Fifth, our software forge is GitLab: ( from gitlab.com )

"GitLab is the open DevOps platform, delivered as a single application."
  
We're going to use all these fancy technologies together to do something seemingly simple:

Publish a website.  

At a high level, here's how everything works together:
 - We use GitLab to host our Git repo and CI/CD pipeline, along with our secrets and keys.
 - Our pipeline ingests HTML and assets from a traditional publishing server and processes dynamic content into static files.
 - Static files are pinned on IPFS and hashes are published on IPNS using the key we blessed on ENS.

With all those working together correctly, we have a working Web3 site!

Anyone can access it using a Web3 browser, or through a gateway.

-----

“When we hold our own keys, None can expel us from paradise.”


